Description :
The project to automate login to Google playstore,add apps to wishlist,install new app from wishlist, indicate the apps installed using Appium through the mobile phone or using MyApps in chrome to verify the app was installed.

Problems:
-Google using dynamic web elements that change every page load with random IDs and they really work to prevent automation tools from automate the PlayStore.
-Login with gmail account many times using automation tool will restrice the access and ask for mobile code verification.

Prerequisites:
-Remove mobile and alt email as recovery methods from google account will be used.
-Install appium for windows and it's environment variables.
-Java

Reporting:
-To generate report using "allure" add the allure bin folder path to your environment variable like : C:\Users\bmahmoud\eclipse-workspace\Trufla\src\main\resources\allure\bin
-To open the generated allure reports use the command : allure serve {your_allure_result_path} like : C:\Users\bmahmoud\eclipse-workspace\Trufla\allure-results