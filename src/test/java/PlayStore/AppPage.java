package PlayStore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AppPage {
	// init
	WebDriver Driver;
	String packageName;
	// locators
	By categoryMenuLi = By.xpath("//ul[@role = 'navigation']/li/a[@href = '/store/apps']");
	By wishListButton = By.xpath("//wishlist-add");
	By installButton = By.xpath("//button[@aria-label='Install']");
	By deviceMenu = By.xpath("//button[contains(@class, 'device-selector-button')]");
	By deviceSelected = By.xpath("(//button[contains(@class, 'dropdown-child')])[1]");
	By purchaseButton = By.xpath("//*[@id=\"base-dialog-body-content\"]/div//button[@id='purchase-ok-button']");
	By appTitle = By.xpath("//h1[@itemprop='name']//span");

	By reviewStart = By.xpath("//span[text()=' Write a review']");
	By starsButton = By.xpath("//div[@class='review-row star-rating-row-desktop']//button[@data-star-index='5']");
	By reviewTextArea = By.xpath("//div[@class='write-review-comment-container']//textarea");
	By reviewSubmit = By.xpath("//div[@class='review-action-button-container']//button[text()= 'SUBMIT']");

	public AppPage(WebDriver MyDriver) {
		// TODO Auto-generated constructor stub
		Driver = MyDriver;
	}

	public String getAppTitle() {
		String title = Driver.findElement(appTitle).getText();
		return title;
	}

	public void addToWishList() {
		Driver.findElement(wishListButton).click();
	}

	public void installApp() {
		packageName = (Driver.getCurrentUrl().toString()).split("=")[1];

		Driver.findElement(installButton).click();
		WebDriverWait wait = new WebDriverWait(Driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bassam939393']")));
		Driver.findElement(deviceMenu).click();
		Driver.findElement(deviceSelected).click();
		Driver.findElement(purchaseButton).click();
	}

	public void rateGame(String rate) {
		Driver.findElement(By.xpath("//a[@title='" + rate + "']")).click();
		Driver.findElement(reviewStart).click();
		Driver.findElement(starsButton).click();
		Driver.findElement(reviewTextArea).sendKeys("Test");
		Driver.findElement(reviewSubmit).click();

	}

}
