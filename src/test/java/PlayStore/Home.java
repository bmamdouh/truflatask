package PlayStore;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Home {
	//init
	WebDriver Driver;
	String appStoreLink = "https://play.google.com/";
	String FirstGame;
	String LastGame;
	
	//locators
	By signInButton = By.xpath("//a[@target='_top' or text() = 'Sign in']");
	By categoryMenuLi = By.xpath("//ul[@role = 'navigation']/li/a[@href = '/store/apps']");
	By fGame = By.xpath("((//h2[contains(text(), 'Top rated games' ) ])[1]/../../../..//a[@class = 'poRVub'])[1]");
	By lGame = By.xpath("((//h2[contains(text(), 'Top rated games' ) ])[1]/../../../..//a[@class = 'poRVub'])[4]");

	By wishList = By.xpath("//ul[@role = 'navigation']/following-sibling::div//a[contains(@href, 'wishlist')]");
	
	
	public Home(WebDriver MyDriver) {
		// TODO Auto-generated constructor stub
		Driver = MyDriver;
	}
	public void openApps() {
		
		Driver.navigate().to(appStoreLink);
		Driver.findElement(categoryMenuLi).click();
	}
	public void getFirstGame() {
		WebDriverWait wait = new WebDriverWait(Driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='gb_ya gbii']")));
		Driver.navigate().to("https://play.google.com/store/apps/category/GAME");
		//Driver.findElement(By.xpath("(//li[@class='secondary-sub-nav-option'])[1]//a")).click();
		Driver.findElement(By.xpath("(//div[@class='card no-rationale square-cover apps small'])[1]")).click();
	}
	public void getLastGame() {
		//Driver.findElement(By.xpath("(//li[@class='secondary-sub-nav-option'])[1]//a")).click();
		Driver.navigate().to("https://play.google.com/store/apps/category/GAME");
		//int count = Driver.findElements(By.xpath("//div[@class='card no-rationale square-cover apps small']")).size();
		Driver.findElement(By.xpath("(//div[@class='card no-rationale square-cover apps small'])[4]")).click();
	}

}
