package PlayStore;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class Login {
	// init
	WebDriver Driver;
	Home myHome;
	// locators
	By emailInput = By.xpath("//input[@type='email']");
	By nextLoginStepEmail = By.xpath("//div[@id='identifierNext']");
	By passInput = By.xpath("//input[@type='password' and @name='password']");
	By nextLoginStepPass = By.xpath("//div[@id='passwordNext']");

	public Login(WebDriver MyDriver) {
		// TODO Auto-generated constructor stub
		Driver = MyDriver;
	}

	public void loginin(String Email, String Pass) {
		myHome = new Home(Driver);
		Driver.findElement(myHome.signInButton).click();
		Driver.findElement(emailInput).sendKeys(Email);
		Driver.findElement(nextLoginStepEmail).sendKeys(Keys.ENTER);
		Driver.findElement(passInput).sendKeys(Pass);
		Driver.findElement(passInput).sendKeys(Keys.ENTER);
		System.out.println("Logged In with gmail account successfully!");
	}

}
