package PlayStore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class MyApps {
	//init
	WebDriver Driver;
	String URL = "https://play.google.com/apps";
	
	//locators
	By appTitle = By.xpath("(//div[contains(@class, 'id-card-list')]//div[contains(@class, 'card-content')])[1]//a[@class='title']");
	
	public MyApps(WebDriver MyDriver) {
		// TODO Auto-generated constructor stub
		Driver = MyDriver;
	}
	public void openMyApps(String app) {
		Driver.navigate().to(URL);
		Assert.assertEquals(true, Driver.findElement(By.xpath("//a[@title='"+app+"']")).isDisplayed());
	}
	
}
