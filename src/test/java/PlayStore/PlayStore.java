package PlayStore;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import TruflaAppium.Appium;

public class PlayStore {
	// init
	WebDriver MyDriver;
	String fGame;
	String lGame;
	String installedGame;
	Home homePage;
	Login login;
	AppPage apps;
	WishList wishlist;
	Appium appium;
	MyApps myApp;

	@BeforeClass
	public void initiatChrome() {
		System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
		MyDriver = new ChromeDriver();
		System.out.print("Chrome browser initialized and Google website opened");
		MyDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		homePage = new Home(MyDriver);
		apps = new AppPage(MyDriver);
	}

	@Test(priority = 1)
	public void openAppStore() {
		homePage.openApps();
		login = new Login(MyDriver);
		login.loginin("mamdouh.mahmoud7400@gmail.com", "Mamdouh23962");
	}

	@Test(description = "get the first game in the list and add it to wishlist", priority = 2)
	public void firstGame() {
		homePage = new Home(MyDriver);
		homePage.getFirstGame();
		fGame = apps.getAppTitle();
		apps.addToWishList();
	}

	@Test(description = "get the last game in the list and add it to wishlist", priority = 3)
	public void lastGame() {
		homePage.getLastGame();
		lGame = apps.getAppTitle();
		apps.addToWishList();
	}

	@Test(description = "open first game added to the wishlist", priority = 4)
	public void openAppFromWishList() {
		wishlist = new WishList(MyDriver);
		wishlist.openGame();
	}

	@Test(description = "install the first game", priority = 5)
	public void installGame() {
		apps = new AppPage(MyDriver);
		installedGame = apps.getAppTitle();
		apps.installApp();
	}

	@Test(description = "indicate if game was installed successfully with appium", priority = 6)
	public void indicateInstalledAppium() {
		appium = new Appium("DeviceID", "8.1.0", apps.packageName);
	}

	@Test(description = "indicate if game was installed successfully from MyApps list", priority = 7)
	public void indicateInstalledMyApps() {
		myApp = new MyApps(MyDriver);
		myApp.openMyApps(installedGame);
	}

	@Test(description = "rate the game and write the review", priority = 8)
	public void rateGame() {
		apps.rateGame(installedGame);
	}

	@AfterClass
	public void closeBrowser() {
		MyDriver.quit();
		System.out.println("Browser session closed");
	}
}
