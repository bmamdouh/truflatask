package PlayStore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WishList {
	//init
	WebDriver Driver;
	String URL = "https://play.google.com/wishlist";
	String myGame;		
	//locators
	By gameList = By.xpath("(//div[contains(@class, 'id-card-list')]//div[contains(@class, 'card-content')]/a)[1]");
	By game = By.xpath("(//div[@class='cover']//a[@class='card-click-target'])[1]");
	public WishList(WebDriver MyDriver) {
		// TODO Auto-generated constructor stub
		Driver = MyDriver;
		
	}
	public void openGame() {
		Driver.navigate().to("https://play.google.com/wishlist");
		Driver.findElement(game).click();
	}

}
