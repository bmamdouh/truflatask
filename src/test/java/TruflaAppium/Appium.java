package TruflaAppium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Appium {
	DesiredCapabilities caps;
	public Appium(String UDID, String androidVersion, String PackageName) {
		caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "My Phone");
		caps.setCapability("udid", UDID); // Give Device ID of your mobile phone
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", androidVersion);
		caps.setCapability("appPackage", PackageName);
		caps.setCapability("appActivity", "com.google.android.finsky.activities.MainActivity");
		caps.setCapability("noReset", "true");

	}

	public boolean searchApp() {
		try {
			AppiumDriver<MobileElement> driver = new AndroidDriver<MobileElement>(
					new URL("http://localhost:4723/wd/hub"), caps);
			return true;
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
			return false;
		}

	}
}
